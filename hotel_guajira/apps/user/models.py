from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
# Create your models here.



class Employee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to='profiles', null=True, blank=True)
    is_admin = models.BooleanField(default=False)
    address = models.CharField(max_length=120, verbose_name="Direccion")
    #section


    class Meta:
        ordering = ['user__username']


class Client(models.Model):
    avatar = models.ImageField(upload_to='profiles', null=True, blank=True)
    cel = models.CharField(max_length=12, verbose_name="Celular")
    address = models.CharField(max_length=120, verbose_name="Dirección")
    email = models.EmailField(max_length=20)
    #Bookings
